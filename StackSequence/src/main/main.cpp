#include "StackSequence.h"

int main() {
    auto solution = StackSequence(true);
    bool valid = solution.validateStackSequences({1, 0}, {1, 0});
    std::cout << "Is valid? " << (valid ? " Yes" : " No") << std::endl;
    return 0;
}
