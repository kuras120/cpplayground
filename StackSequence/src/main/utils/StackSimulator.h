//
// Created by wojci on 15.04.2023.
//

#ifndef TESTS_STACKSIMULATOR_H
#define TESTS_STACKSIMULATOR_H


#include <vector>
#include <ctime>
#include <cstdlib>
#include <stack>

class StackSimulator {
public:
    std::vector<int> pushed;
    std::vector<int> popped;
    void generate(int size, int seed) {
        srand(seed);
        std::stack<int> stack;
        for (int i = 0; i < size; i++) {
            if (rand() % 2 == 0 || stack.empty()) {
                int randomNumber = rand() % 100;
                stack.push(randomNumber);
                pushed.push_back(randomNumber);
            } else {
                popped.push_back(stack.top());
                stack.pop();
            }
        }
    }
};


#endif //TESTS_STACKSIMULATOR_H
