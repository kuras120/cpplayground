//
// Created by wojci on 14.04.2023.
//

#ifndef TESTS_STACKSEQUENCE_H
#define TESTS_STACKSEQUENCE_H


#include <vector>
#include <algorithm>
#include <iostream>
#include <stack>

class StackSequence {
private:
    bool debug;
    void printVec(std::vector<int> vector, std::string name) {
        if (this->debug) {
            std::cout << name << std::endl;
            for (auto num: vector) {
                std::cout << num << " ";
            }
            std::cout << std::endl;
        }
    }
    void printLine() {
        if (this->debug) std::cout << "---" << std::endl;
    }
    int getIndex(std::vector<int> vector, int element) {
        auto it = std::find(vector.begin(), vector.end(), element);
        if (it != vector.end()) {
            return it - vector.begin();
        } else {
            return -1;
        }
    }
public:
    explicit StackSequence(bool debug) {
        this->debug = debug;
    }
    bool validateStackSequencesOld(std::vector<int> pushed, std::vector<int> popped) {
        auto index = getIndex(pushed, popped[0]);
        std::vector<int> leftPart(pushed.begin(), pushed.begin() + index + 1);
        std::vector<int> rightPart(pushed.begin() + index + 1, pushed.end());
        printVec(leftPart, "leftPart");
        printVec(rightPart, "rightPart");
        printVec(popped, "popped");
        while (!popped.empty()) {
            printLine();
            if (!leftPart.empty() && leftPart.back() == popped.front()) {
                leftPart.pop_back();
                popped.erase(popped.begin());
            } else {
                if (rightPart.empty()) return false;
                leftPart.push_back(rightPart.front());
                rightPart.erase(rightPart.begin());
            }
            printVec(leftPart, "leftPart");
            printVec(rightPart, "rightPart");
            printVec(popped, "popped");
        }
        return true;
    }

    bool validateStackSequences(std::vector<int> pushed, std::vector<int> popped) {
        std::stack<int> stack;
        int i = 1, j = 0;
        stack.push(pushed.front());
        while (j < popped.size()) {
            if (!stack.empty() && stack.top() == popped[j]) {
                stack.pop(); ++j;
            } else {
                if (i >= pushed.size()) return false;
                stack.push(pushed[i]); ++i;
            }
        }
        return true;
    }
};


#endif //TESTS_STACKSEQUENCE_H
