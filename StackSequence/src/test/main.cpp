//
// Created by wojci on 14.04.2023.
//
#include <gtest/gtest.h>
#include "../main/StackSequence.h"
#include "../main/utils/StackSimulator.h"

TEST(QuickTest, ValidStackNumbers) {
    auto solution = StackSequence(true);
    bool valid = solution.validateStackSequences({1,2,3,4,5}, {4,5,3,2,1});
    EXPECT_TRUE(valid);
}

TEST(QuickTest, ValidStackNumbersWithTwoElements) {
    auto solution = StackSequence(true);
    bool valid = solution.validateStackSequences({1,0}, {1,0});
    EXPECT_TRUE(valid);
}

TEST(SlowTest, ValidStackNumbers) {
    auto solution = StackSequence(false);
    auto simulator = StackSimulator();
    simulator.generate(500, 2137);
    bool valid = solution.validateStackSequences(simulator.pushed, simulator.popped);
    EXPECT_TRUE(valid);
}

TEST(QuickTest, InvalidStackNumbers) {
    auto solution = StackSequence(true);
    bool invalid = solution.validateStackSequences({1,2,3,4,5}, {4,3,5,1,2});
    EXPECT_FALSE(invalid);
}

TEST(SlowTest, InvalidStackNumbers) {
    auto solution = StackSequence(false);
    bool invalid = solution.validateStackSequences(
            {2, 5, 1, 7, 4, 9, 10, 6, 8, 3, 12, 11, 13, 16, 15, 14, 17, 18, 20, 19, 21, 22, 23, 24, 25},
            {9, 18, 21, 11, 5, 25, 20, 12, 24, 7, 22, 8, 1, 16, 14, 2, 19, 6, 17, 10, 3, 15, 23, 4, 13}
    );
    EXPECT_FALSE(invalid);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
