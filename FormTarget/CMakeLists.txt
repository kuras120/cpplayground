# Add source code and executable to project
add_executable(FormTarget src/main/main.cpp)

# Add test code to project
add_executable(FormTargetTests src/test/main.cpp)
target_include_directories(FormTargetTests PRIVATE ${GTEST_INCLUDE_DIRS})
target_link_libraries(FormTargetTests ${GTEST_BOTH_LIBRARIES} pthread)
add_test(NAME UnitTests COMMAND FormTargetTests)
